using System;

namespace WF.Business.Model
{
    public class aspnet_Users
    {
        public Guid UserId { get; set; }
        public Guid ApplicationId { get; set; }
        public string UserName { get; set; }
        public string LoweredUserName { get; set; }
        public string MobileAlias { get; set; }
        public bool IsAnonymous { get; set; }
        public DateTime LastActivityDate { get; set; }
        public int Type { get; set; }
        public string FinanceCode { get; set; }
    }
}
