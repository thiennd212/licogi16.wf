using System;

namespace WF.Business.Model
{
    public class lcg_NgayNghi
    {
        public Guid Id { get; set; }
        public DateTime Ngay { get; set; }
        public bool TrangThai { get; set; }
        public string MoTa { get; set; }
        public Guid CreatedByUserId { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public Guid LastModifiedByUserId { get; set; }
        public DateTime LastModifiedOnDate { get; set; }
    }
}
