using System;

namespace WF.Business.Model
{
    public class aspnet_Roles
    {
        public Guid RoleId { get; set; }
        public Guid ApplicationId { get; set; }
        public string RoleName { get; set; }
        public string LoweredRoleName { get; set; }
        public string Description { get; set; }
        public bool EnableDelete { get; set; }
        public Guid CreatedByUserID { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public Guid LastModifiedByUserID { get; set; }
        public DateTime LastModifiedOnDate { get; set; }
        public string RoleCode { get; set; }
        public int? UyQuyenChoNguoiKhac { get; set; }
        public int? XemBaoCaoTraCuuThongKe { get; set; }
        public int? XemThongKe { get; set; }
    }
}
