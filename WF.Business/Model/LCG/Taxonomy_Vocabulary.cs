using System;

namespace WF.Business.Model
{
    public class Taxonomy_Vocabulary
    {
        public Guid VocabularyId { get; set; }
        public Guid VocabularyTypeID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Weight { get; set; }
        public Guid CreatedByUserID { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public Guid LastModifiedByUserId { get; set; }
        public DateTime LastModifiedOnDate { get; set; }
        public bool IsSystem { get; set; }
        public Guid ApplicationId { get; set; }
        public string Code { get; set; }
    }
}
