using System;

namespace WF.Business.Model
{
    public class SystemParameter
    {
        public Guid ParameterId { get; set; }
        public Guid ApplicationId { get; set; }
        public string ParameterCode { get; set; }
        public string Description { get; set; }
        public string ParameterValue { get; set; }
        public Guid CreatedByUserId { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public Guid LastModifiedByUserId { get; set; }
        public DateTime LastModifiedOnDate { get; set; }
    }
}
