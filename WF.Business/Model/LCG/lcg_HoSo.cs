using System;

namespace WF.Business.Model
{
    public class lcg_HoSo
    {
        public Guid HoSoId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public Guid QuyTrinhId { get; set; }
        public Guid? DuAnId { get; set; }
        public string NhaCungCap { get; set; }
        public Guid? NhaThauId { get; set; }
        public DateTime? NgayBatDauCongTac { get; set; }
        public DateTime? NgayKetThucCongTac { get; set; }
        public string NoiDenCongTac { get; set; }
        public string NoiDiCongTac { get; set; }
        public Guid? PhuongTienId { get; set; }
        public Guid CurrentUserId { get; set; }
        public Guid PreUserId { get; set; }
        public DateTime? BeginDateState { get; set; }
        public DateTime? EndDateState { get; set; }
        public int? IsFinished { get; set; }
        public DateTime NgayNhap { get; set; }
        public Guid CreatedByUserId { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public Guid LastModifiedByUserId { get; set; }
        public DateTime LastModifiedOnDate { get; set; }
    }
}
