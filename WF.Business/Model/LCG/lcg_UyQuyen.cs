using System;

namespace WF.Business.Model
{
    public class lcg_UyQuyen
    {
        public Guid Id { get; set; }
        public Guid NguoiUyQuyen { get; set; }
        public Guid NguoiDuocUyQuyen { get; set; }
        public DateTime TuNgay { get; set; }
        public DateTime DenNgay { get; set; }
        public bool TrangThai { get; set; }
        public Guid CreatedByUserId { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public Guid LastModifiedByUserId { get; set; }
        public DateTime LastModifiedOnDate { get; set; }
    }
}
