using System;

namespace WF.Business.Model
{
    public class Taxonomy_Term
    {
        public Guid TermId { get; set; }
        public Guid VocabularyId { get; set; }
        public Guid? ParentId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Weight { get; set; }
        public Guid TermLeft { get; set; }
        public Guid TermRight { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public Guid? LastModifiedByUserId { get; set; }
        public DateTime LastModifiedOnDate { get; set; }
        public Guid ApplicationId { get; set; }
        public int Order { get; set; }
        public string VocabularyCode { get; set; }
        public int? ChildCount { get; set; }
        public string IdPath { get; set; }
        public string Path { get; set; }
        public int? Level { get; set; }
    }
}
