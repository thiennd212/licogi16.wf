﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WF.Business.DataAccess
{
    public interface ICommandStateRepository
    {
        List<Model.Taxonomy_Term> GetAllCommand();
        List<Model.Taxonomy_Term> GetAllState();
    }
}
