namespace WF.MsSql
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;


    public partial class LCGContext : DbContext
    {
        private readonly IConfiguration _config;

        public LCGContext(IConfiguration config)
        {
            _config = config;
        }

        public virtual DbSet<aspnet_Roles> aspnet_Roles { get; set; }
        public virtual DbSet<aspnet_Users> aspnet_Users { get; set; }
        public virtual DbSet<aspnet_Membership> aspnet_Memberships { get; set; }
        public virtual DbSet<aspnet_UsersInRoles> aspnet_UsersInRoles { get; set; }
        public virtual DbSet<Taxonomy_Vocabulary> Taxonomy_Vocabularies { get; set; }
        public virtual DbSet<Taxonomy_Term> Taxonomy_Terms { get; set; }
        public virtual DbSet<lcg_HoSo> lcg_HoSos { get; set; }
        public virtual DbSet<lcg_UyQuyen> lcg_UyQuyen { get; set; }
        public virtual DbSet<lcg_NgayNghi> lcg_NgayNghi { get; set; }
        public virtual DbSet<SystemParameter> SystemParameter { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_config.GetConnectionString("LCGConnection"));
            }

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<aspnet_UsersInRoles>()
                .HasKey(x => new { x.RoleId, x.UserId, x.ApplicationId });
        }
    }
}
