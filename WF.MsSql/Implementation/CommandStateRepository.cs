﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WF.Business;
using WF.Business.DataAccess;
using WF.Business.Model;

namespace WF.MsSql.Implementation
{
    public class CommandStateRepository : ICommandStateRepository
    {
        private readonly LCGContext _lcgContext;

        public CommandStateRepository(LCGContext lcgContext)
        {
            _lcgContext = lcgContext;
        }

        public List<Business.Model.Taxonomy_Term> GetAllCommand()
        {
            try
            {
                var data =(from a in _lcgContext.Taxonomy_Vocabularies
                           join b in _lcgContext.Taxonomy_Terms
                           on a.VocabularyId equals b.VocabularyId
                           where a.Code == CONSTANT.TAX_COMMAND
                           orderby b.Name select b)
                           .ToList()
                           .Select(e => Mappings.Mapper.Map<Business.Model.Taxonomy_Term>(e)).ToList();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Business.Model.Taxonomy_Term> GetAllState()
        {
            try
            {
                var tax_voca = _lcgContext.Taxonomy_Vocabularies.FirstOrDefault(x => x.Code == CONSTANT.TAX_STATE);
                var tax_term = _lcgContext.Taxonomy_Terms.Where(x => x.VocabularyId == tax_voca.VocabularyId).ToList();                           
                var data = tax_term.Select(e => Mappings.Mapper.Map<Business.Model.Taxonomy_Term>(e)).ToList();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
