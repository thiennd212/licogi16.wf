namespace WF.MsSql
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("lcg_NgayNghi")]
    public partial class lcg_NgayNghi
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public DateTime Ngay { get; set; }

        [Required]
        public bool TrangThai { get; set; }

        public string MoTa { get; set; }

        [Required]
        public Guid CreatedByUserId { get; set; }

        [Column(TypeName = "datetime"), Required]
        public DateTime CreatedOnDate { get; set; }

        [Required]
        public Guid LastModifiedByUserId { get; set; }

        [Column(TypeName = "datetime"), Required]
        public DateTime LastModifiedOnDate { get; set; }
    }
}
