namespace WF.MsSql
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    [Table("Taxonomy_Vocabulary")]
    public partial class Taxonomy_Vocabulary
    {
        [Key]
        public Guid VocabularyId { get; set; }

        [Required]
        public Guid VocabularyTypeID { get; set; }

        [Required]
        [StringLength(256)]
        public string Name { get; set; }

        public string Description { get; set; }

        [Required]        
        public int Weight { get; set; }        

        public Guid CreatedByUserID { get; set; }

        public DateTime CreatedOnDate { get; set; }

        public Guid LastModifiedByUserId { get; set; }

        public DateTime LastModifiedOnDate { get; set; }

        [Required]
        public bool IsSystem { get; set; }

        [Required]
        public Guid ApplicationId { get; set; }

        [StringLength(256)]
        public string Code { get; set; }
    }
}
