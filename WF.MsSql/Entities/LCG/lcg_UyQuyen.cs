namespace WF.MsSql
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("lcg_UyQuyen")]
    public partial class lcg_UyQuyen
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public Guid NguoiUyQuyen { get; set; }

        [Required]
        public Guid NguoiDuocUyQuyen { get; set; }

        [Required]
        public DateTime TuNgay { get; set; }

        [Required]
        public DateTime DenNgay { get; set; }

        [Required]
        public bool TrangThai { get; set; }

        [Required]
        public Guid CreatedByUserId { get; set; }

        [Column(TypeName = "datetime"), Required]
        public DateTime CreatedOnDate { get; set; }

        [Required]
        public Guid LastModifiedByUserId { get; set; }

        [Column(TypeName = "datetime"), Required]
        public DateTime LastModifiedOnDate { get; set; }
    }
}
