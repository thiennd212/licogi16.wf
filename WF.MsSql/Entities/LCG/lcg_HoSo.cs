namespace WF.MsSql
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("lcg_HoSo")]
    public partial class lcg_HoSo
    {
        [Key]
        public Guid HoSoId { get; set; }

        [StringLength(1024), Required]
        public string Code { get; set; }

        [StringLength(3000), Required]
        public string Name { get; set; }

        [Required]
        public Guid QuyTrinhId { get; set; }

        public Guid? DuAnId { get; set; }

        public string NhaCungCap { get; set; }

        public Guid? NhaThauId { get; set; }

        public DateTime? NgayBatDauCongTac { get; set; }

        public DateTime? NgayKetThucCongTac { get; set; }

        public string NoiDenCongTac { get; set; }

        public string NoiDiCongTac { get; set; }

        public Guid? PhuongTienId { get; set; }

        [Required]
        public Guid CurrentUserId { get; set; }

        [Required]
        public Guid PreUserId { get; set; }

        public DateTime? BeginDateState { get; set; }

        public DateTime? EndDateState { get; set; }

        public int? IsFinished { get; set; }

        [Column(TypeName = "datetime"), Required]
        public DateTime NgayNhap { get; set; }

        [Required]
        public Guid CreatedByUserId { get; set; }

        [Column(TypeName = "datetime"), Required]
        public DateTime CreatedOnDate { get; set; }

        [Required]
        public Guid LastModifiedByUserId { get; set; }

        [Column(TypeName = "datetime"), Required]
        public DateTime LastModifiedOnDate { get; set; }
    }
}
