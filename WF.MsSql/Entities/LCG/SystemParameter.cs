namespace WF.MsSql
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("SystemParameter")]
    public partial class SystemParameter
    {
        [Key]
        [Column("ParameterID")]
        public Guid ParameterId { get; set; }
        [Column("ApplicationID")]
        public Guid ApplicationId { get; set; }
        [Required]
        [StringLength(64)]
        public string ParameterCode { get; set; }
        [StringLength(512)]
        public string Description { get; set; }
        [Required]
        public string ParameterValue { get; set; }
        [Column("CreatedByUserID")]
        public Guid CreatedByUserId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime CreatedOnDate { get; set; }
        [Column("LastModifiedByUserID")]
        public Guid LastModifiedByUserId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime LastModifiedOnDate { get; set; }
    }
}
