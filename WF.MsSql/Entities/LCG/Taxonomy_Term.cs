namespace WF.MsSql
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    [Table("Taxonomy_Term")]
    public partial class Taxonomy_Term
    {
        [Key]
        public Guid TermId { get; set; }

        [Required]
        public Guid VocabularyId { get; set; }

        public Guid? ParentId { get; set; }

        [Required]
        [StringLength(256)]
        public string Name { get; set; }
                       
        public string Description { get; set; }  
        
        [Required]
        public int Weight { get; set; }

        [Required]
        public Guid TermLeft { get; set; }

        [Required]
        public Guid TermRight { get; set; }

        public Guid? CreatedByUserId { get; set; }

        public DateTime CreatedOnDate { get; set; }

        public Guid? LastModifiedByUserId { get; set; }

        public DateTime LastModifiedOnDate { get; set; }
        
        [Required]
        public Guid ApplicationId { get; set; }
                
        public int? Order { get; set; }

        [StringLength(256)]
        public string VocabularyCode { get; set; }

        public int ChildCount { get; set; }

        [StringLength(900)]
        public string IdPath { get; set; }

        [StringLength(450)]
        public string Path { get; set; }

        public int? Level { get; set; }
    }
}
